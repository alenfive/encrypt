package com.source3g.hermes.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

/**
 * <p>重写response,以此来获取，及重写response body等的内容.</p>
 * <p>创建时间  2015年12月31日下午7:05:34</p>
 * <p>Copyright ©2016 国盛天丰科技有限公司</p>
 * @version v1.0
 * @author mihuajun
 */

public class WrapperedResponse extends HttpServletResponseWrapper {
	
	/**
	 * <p>真正存储数据的流 </p>
	 */
	private ByteArrayOutputStream buffer = null; 
	
	private int code = SC_OK;
	
	private ServletOutputStream out = null; 
	private PrintWriter writer = null;

	public WrapperedResponse(HttpServletResponse response) {
		super(response);
		try {
			buffer = new ByteArrayOutputStream();
			out = new WrapperedServletOutputStream(buffer);
			writer = new PrintWriter(new OutputStreamWriter(buffer, this.getCharacterEncoding()));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}

	/**
	 * <p>重写OutputStream输出位置.</p>
	 * <p>作者  [mihuajun]</p>
	 * <p>创建时间  2015年12月31日下午7:10:30</p>
	 * @return
	 * @throws IOException
	 */
	@Override
	public ServletOutputStream getOutputStream() throws IOException {
		// TODO Auto-generated method stub
		return out;
	}

	/**
	 * <p>重写Writer输出位置.</p>
	 * <p>作者  [mihuajun]</p>
	 * <p>创建时间  2015年12月31日下午7:10:30</p>
	 * @return
	 * @throws IOException
	 */
	@Override
	public PrintWriter getWriter() throws IOException {
		// TODO Auto-generated method stub
		return writer;
	}
	

	/**
	 * <p>刷新缓存.</p>
	 * <p>作者  [mihuajun]</p>
	 * <p>创建时间  2016年1月4日上午10:21:49</p>
	 * @throws IOException
	 */
	
	@Override
	public void flushBuffer() throws IOException {
		// TODO Auto-generated method stub
		if (out != null) { 
			out.flush(); 
		} 
		if (writer != null) { 
			writer.flush(); 
		} 
	}
	
	public boolean isFound(){
        return code != SC_NOT_FOUND;
    }

	/**
	 * <p>方法说明概要.</p>
	 * <p>方法说明详细</p>
	 * <p>作者  [mihuajun,...]</p>
	 * <p>创建时间  2016年1月5日上午10:49:05</p>
	 * @param sc
	 * @param msg
	 * @throws IOException
	 */
	
	@Override
	public void sendError(int sc, String msg) throws IOException {
		// TODO Auto-generated method stub
		this.code = sc;
		if(isFound()){
            super.sendError(sc,msg);
        }else{
            super.setStatus(SC_OK);                 
        }
	}

	/**
	 * <p>方法说明概要.</p>
	 * <p>方法说明详细</p>
	 * <p>作者  [mihuajun,...]</p>
	 * <p>创建时间  2016年1月5日上午10:49:05</p>
	 * @param sc
	 * @throws IOException
	 */
	
	@Override
	public void sendError(int sc) throws IOException {
		// TODO Auto-generated method stub
		this.code = sc;
        if(isFound()){
            super.sendError(sc);                
        }else{
            super.setStatus(SC_OK);
        }
	}


	/**
	 * <p>清空缓存.</p>
	 * <p>作者  [mihuajun]</p>
	 * <p>创建时间  2016年1月4日上午10:22:26</p>
	 */
	
	@Override
	public void reset() {
		// TODO Auto-generated method stub
		buffer.reset(); 
	}
	
	/**
	 * 
	 * <p>返回response输出的字节数组类型内容.</p>
	 * <p>作者  [mihuajun,...]</p>
	 * <p>创建时间  2016年1月4日上午11:20:47</p>
	 * @return
	 * @throws IOException
	 */
	public byte[] getResponseData() throws IOException { 
		flushBuffer(); 
		return buffer.toByteArray(); 
	} 
	
	/**
	 * <p>重写servletOutputStream流.</p>
	 * <p>创建时间  2016年1月4日上午11:21:28</p>
	 * <p>Copyright ©2016 国盛天丰科技有限公司</p>
	 * @version v1.0
	 * @author mihuajun
	 * @author 其他作者
	 */
	public class WrapperedServletOutputStream extends ServletOutputStream{
		
		ByteArrayOutputStream buffer;
		
		public WrapperedServletOutputStream(ByteArrayOutputStream buffer){
			this.buffer = buffer;
		}
		
		@Override
		public void write(int b) throws IOException {
			// TODO Auto-generated method stub
			buffer.write(b);
		}

		
		@Override
		public void write(byte[] b) throws IOException {
			// TODO Auto-generated method stub
			buffer.write(b);
		}
		@Override
		public void write(byte[] b, int off, int len) throws IOException {
			// TODO Auto-generated method stub
			buffer.write(b, off, len);
		}
		
		
		
	}
}
