package com.source3g.hermes.utils;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.spec.X509EncodedKeySpec;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.crypto.Cipher;

import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.Base64Utils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@SuppressWarnings("unused")
public class RSARestTemplate extends RestTemplate {
	
	private final Logger logger  =  Logger.getLogger(RSARestTemplate.class );
	
	private ConcurrentHashMap<String,KeyEntity> serviceKey = new ConcurrentHashMap<String,KeyEntity>();
	
	private Set<String> encryptServices = new HashSet<String>();
	
	public static final String KEY_ALGORITHM = "RSA";
	
	private final String AES_KEY_NAME = "AES_KEY";
	private final String RSA_KEY_NAME = "RSA_KEY";
	private final String CIPHERTEXT_NAME = "ciphertext";
	
	private final String GET_RSA_KEY_ACTION = "GET_RSA_KEY";
	private final String SEND_AES_KEY_ACTION = "SEND_AES_KEY";
	private final String SEND_CIPHERTEXT_ACTION = "SEND_CIPHERTEXT";
	private final String ENCRYPT_ACTION= "ENCRYPT";
	
	private final String ENCRYPT_SUCCESS = "ENCRYPT_SUCCESS";
	private final String ENCRYPT_FAIL = "ENCRYPT_FAIL";
	

	public RSARestTemplate() {
		// TODO Auto-generated constructor stub
		super(new HttpComponentsClientHttpRequestFactory());
	}

	/**
	 * <p 方法说明概要.</p>
	 * <p>方法说明详细</p>
	 * <p>作者  [mihuajun,...]</p>
	 * <p>创建时间  Mar 24, 20166:54:29 PM</p>
	 * @param url
	 * @param request
	 * @param responseType
	 * @return
	 * @throws RestClientException
	 */
	@SuppressWarnings({"rawtypes" })
	@Override
	public <T> T postForObject(URI url, Object request, Class<T> responseType) throws RestClientException {
		// TODO Auto-generated method stub
		
		
		//不在加密服务器列表的url请求，不作处理
		String encryptService = indexOfEncryptServices(url);
		KeyEntity keyEntity = null;
		if(encryptService == null || (keyEntity = getKeyEntity(encryptService,url)) == null){
			logger.debug("no encrypt access #"+url.toString() );
			return super.postForObject(url, request, responseType);
		}
		
		String aesKey = keyEntity.getAesKey();
		
		ObjectMapper mapper = new ObjectMapper();
		
		
		try {
			String jsonData = null;
			if(request instanceof HttpEntity){
				jsonData = mapper.writeValueAsString(((HttpEntity)request).getBody());
			}else{
				jsonData = mapper.writeValueAsString(request);
			}
			
			
			//加密数据
			byte[] encryptResult = AESUtils.encrypt(jsonData, aesKey);  
			String encryptResultStr = AESUtils.parseByte2HexStr(encryptResult);  
			
			MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();
			parts.add(CIPHERTEXT_NAME, encryptResultStr);
			parts.add(ENCRYPT_ACTION, SEND_CIPHERTEXT_ACTION);
			
			String resultJSON = super.postForObject(url, parts, String.class);
			
			logger.debug("result json = "+resultJSON);
			
			Map<String, Object> result = mapper.readValue(resultJSON, new TypeReference<Map<String, Object>>() {}); 
			
			//加密传输数据失败，重新获取密钥,大神保佑，不要死循环
			if(result == null || !ENCRYPT_SUCCESS.equals(result.get("status"))){
				serviceKey.remove(encryptService);
				logger.debug("send ciphertext fail..");
				return this.postForObject(url,request,responseType);
			}
			
			//解密   
			byte[] decryptFrom = AESUtils.parseHexStr2Byte(result.get(CIPHERTEXT_NAME).toString());  
			byte[] decryptResult = AESUtils.decrypt(decryptFrom,aesKey);  
			
			logger.debug("result data:"+new String(decryptResult,"UTF-8"));
			
			
			try {
				return mapper.readValue(decryptResult, responseType);
			} catch (Exception e) {
				// TODO: handle excepion
				return responseType.cast(new String(decryptResult,"UTF-8"));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	private KeyEntity getKeyEntity(String encryptService,URI postUrl) {
		
		
		KeyEntity keyEntity = serviceKey.get(encryptService);
		
		if(keyEntity != null )return keyEntity;
		
		MultiValueMap<String, Object> param = new LinkedMultiValueMap<String, Object>();
		param.add(ENCRYPT_ACTION, GET_RSA_KEY_ACTION);
		String rsaKey = super.postForObject(postUrl,param, String.class);
		
		rsaKey = getRsaKey(rsaKey);
		
		if(rsaKey == null)return null;
		
		String aesKey = generateAESKey();
		param = new LinkedMultiValueMap<String, Object>();
		param.add(ENCRYPT_ACTION, SEND_AES_KEY_ACTION);
		try {
			param.add(AES_KEY_NAME, new String(Base64Utils.encode(encryptByPublicKey(aesKey.getBytes("UTF-8"),rsaKey)),"UTF-8"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
		String aesResponseStatus = super.postForObject(postUrl,param, String.class);
		
		if(!isAesKeyOk(aesResponseStatus))return null;
		
		keyEntity = new KeyEntity();
		keyEntity.setAesKey(aesKey);
		keyEntity.setRsaKey(rsaKey);
		serviceKey.put(encryptService, keyEntity);
		return keyEntity;
		
	}

	private String generateAESKey(){
		return UUID.randomUUID().toString();
	}
	
	public static byte[] encryptByPublicKey(byte[] data, String key)
			throws Exception {
		// 对公钥解密
		byte[] keyBytes = decryptBASE64(key);

		// 取得公钥
		X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		Key publicKey = keyFactory.generatePublic(x509KeySpec);

		// 对数据加密
		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);

		return cipher.doFinal(data);
	}
	
	public static byte[] decryptBASE64(String key){
		return Base64Utils.decode(key.getBytes());
	}
	
	/**
	 * @param encryptServices encryptServices
	 */
	public void setEncryptServices(Set<String> encryptServices) {
		this.encryptServices = encryptServices;
	}
	
	private String getRsaKey(String rsaKey){
		if(rsaKey == null || !rsaKey.startsWith(RSA_KEY_NAME))return null;
		return rsaKey.substring(RSA_KEY_NAME.length()+1);
	}
	
	private boolean isAesKeyOk(String aesResponseStatus){
		if(ENCRYPT_SUCCESS.equals(aesResponseStatus)){
			return true;
		}
		return false;
	}

	private final String indexOfEncryptServices(URI url){
		if(url == null)return null;
		String item = null;
		for( Iterator<String>   it = encryptServices.iterator();  it.hasNext(); )  
		{
			item = it.next();
		    if(url.toString().indexOf(item) > -1){
		    	return item;
		    }
		}   
		return null;
	}
	
	

	class KeyEntity{
		private String rsaKey = null;
		private String aesKey = null;
		/**
		 * @return rsaKey
		 */
		public String getRsaKey() {
			return rsaKey;
		}
		/**
		 * @param rsaKey rsaKey
		 */
		public void setRsaKey(String rsaKey) {
			this.rsaKey = rsaKey;
		}
		/**
		 * @return aesKey
		 */
		public String getAesKey() {
			return aesKey;
		}
		/**
		 * @param aesKey aesKey
		 */
		public void setAesKey(String aesKey) {
			this.aesKey = aesKey;
		}
		
		
	}
	
	public static void main(String[] args) throws URISyntaxException {
		/*RSARestTemplate restTemplate = new RSARestTemplate(new HttpComponentsClientHttpRequestFactory());
		Set<String> serviceList = new HashSet<String>();
		serviceList.add("http://172.16.1.89:8443/cas");
		restTemplate.setEncryptServices(serviceList);
		
		URI postUrl = new URI("http://172.16.1.89:8443/cas/account/exists");
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();
		parts.add("account", "freedom");
		parts.add("password", "root123");
		Map<String, Object> result = restTemplate.postForObject(postUrl, parts, Map.class);
		System.out.println("result:"+result);*/
		
		RSARestTemplate restTemplate = new RSARestTemplate();
		URI url = new URI("http://192.168.71.131:8080/cas/account/add");
		MultiValueMap<String, Object> param = new LinkedMultiValueMap<String, Object>();
		Set<String> serviceList = new HashSet<String>();
		serviceList.add("http://192.168.71.131:8080/cas");
		restTemplate.setEncryptServices(serviceList);
		param.add("accountId", "25999999999910001258");
		param.add("merchantId", "56df86e70cf2824415370e85");
		param.add("account", "account10251");
		param.add("password", "123456");
		param.add("sysCode", "ktv");
		param.add("type", "sub");
		String result = restTemplate.postForObject(url, param, String.class);
		System.out.println(result);
		
		/*RestTemplate template = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
		Map<String, String> param = new HashMap<String, String>();
		param.put("account", "002");
		param.put("pwd", "1234556");
		
		String resultString = template.postForObject("http://localhost:8443/cas/account/exists", new HttpEntity<Map<String, String>>(param), String.class);
		System.out.println(resultString);*/
		
		/*RSARestTemplate restTemplate = new RSARestTemplate();
		URI url = new URI("http://localhost:8443/cas/account/add");
		Map<String, Object> param = new HashMap<String, Object>();
		Set<String> serviceList = new HashSet<String>();
		serviceList.add("http://localhost:8443/cas");
		restTemplate.setEncryptServices(serviceList);
		param.put("accountId", "111111111");
		param.put("merchantId", "111111111");
		param.put("account", "account1000");
		param.put("password", "123456");
		param.put("sysCode", "ktv");
		param.put("type", "sub");
		String result = restTemplate.postForObject(url, new HttpEntity<Map<String, Object>>(param), String.class);
		System.out.println(result);*/
	}
}
