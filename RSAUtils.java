package com.source3g.hermes.utils;

import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;

import org.springframework.util.Base64Utils;

/**
 * <p>RSA非对称加密工具类</p>
 * <p>创建时间  2016年1月6日上午11:07:12</p>
 * <p>Copyright ©2016 国盛天丰科技有限公司</p>
 * @version v1.0
 * @author mihuajun
 */
public class RSAUtils{
	public static final String KEY_ALGORITHM = "RSA";
	public static final String SIGNATURE_ALGORITHM = "MD5withRSA";

	public static final String PUBLIC_KEY = "RSAPublicKey";
	public static final String PRIVATE_KEY = "RSAPrivateKey";


	/**
	 * <p>base64解密</p>
	 * <p>作者  [mihuajun,...]</p>
	 * <p>创建时间  2016年1月6日下午3:12:48</p>
	 * @param key
	 * @return
	 */
	public static byte[] decryptBASE64(String key){
		return Base64Utils.decode(key.getBytes());
	}
	
	/**
	 * <p>base64加密</p>
	 * <p>作者  [mihuajun,...]</p>
	 * <p>创建时间  2016年1月6日下午3:12:48</p>
	 * @param key
	 * @return
	 */
	public static String encryptBASE64(byte[] key){
		return new String(Base64Utils.encode(key));
	}

	
	/**
	 * <p>用公钥解密.</p>
	 * <p>作者  [mihuajun,...]</p>
	 * <p>创建时间  2016年1月6日下午3:12:03</p>
	 * @param data 密文
	 * @param key base64过的私钥
	 * @return 明文
	 * @throws Exception
	 */
	public static byte[] decryptByPublicKey(byte[] data, String key)
			throws Exception {
		// 对密钥解密
		byte[] keyBytes = decryptBASE64(key);

		// 取得公钥
		X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		Key publicKey = keyFactory.generatePublic(x509KeySpec);

		// 对数据解密
		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.DECRYPT_MODE, publicKey);

		return cipher.doFinal(data);
	}

	
	/**
	 * <p>用公钥加密.</p>
	 * <p>作者  [mihuajun,...]</p>
	 * <p>创建时间  2016年1月6日下午3:11:36</p>
	 * @param data 明文
	 * @param key base64过的私钥
	 * @return 密文
	 * @throws Exception
	 */
	public static byte[] encryptByPublicKey(byte[] data, String key)
			throws Exception {
		// 对公钥解密
		byte[] keyBytes = decryptBASE64(key);

		// 取得公钥
		X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		Key publicKey = keyFactory.generatePublic(x509KeySpec);

		// 对数据加密
		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);

		return cipher.doFinal(data);
	}
	
	
	/**
	 * <p>用私钥解密.</p>
	 * <p>作者  [mihuajun,...]</p>
	 * <p>创建时间  2016年1月6日下午3:10:33</p>
	 * @param data 密文
	 * @param key base64过的私钥
	 * @return 明文
	 * @throws Exception
	 */
	public static byte[] decryptByPrivateKey(byte[] data, String key)
			throws Exception {
		// 对密钥解密
		byte[] keyBytes = decryptBASE64(key);

		// 取得私钥
		PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		Key privateKey = keyFactory.generatePrivate(pkcs8KeySpec);

		// 对数据解密
		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.DECRYPT_MODE, privateKey);

		return cipher.doFinal(data);
	}

	/**
	 * <p>用私钥加密</p>
	 * <p>作者  [mihuajun,...]</p>
	 * <p>创建时间  2016年1月6日下午3:09:30</p>
	 * @param data 明文
	 * @param key base64过的私钥
	 * @return 密文
	 * @throws Exception
	 */
	public static byte[] encryptByPrivateKey(byte[] data, String key)
			throws Exception {
		// 对密钥解密
		byte[] keyBytes = decryptBASE64(key);

		// 取得私钥
		PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		Key privateKey = keyFactory.generatePrivate(pkcs8KeySpec);

		// 对数据加密
		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.ENCRYPT_MODE, privateKey);

		return cipher.doFinal(data);
	}

	/**
	 * <p>取得私钥.</p>
	 * <p>作者  [mihuajun,...]</p>
	 * <p>创建时间  2016年1月6日下午3:08:11</p>
	 * @param keyMap
	 * @return base64后的私钥
	 * @throws Exception
	 */
	public static String getPrivateKey(Map<String, Object> keyMap)
			throws Exception {
		Key key = (Key) keyMap.get(PRIVATE_KEY);

		return encryptBASE64(key.getEncoded());
	}

	/**
	 * <p>取得公钥.</p>
	 * <p>作者  [mihuajun,...]</p>
	 * <p>创建时间  2016年1月6日下午3:07:40</p>
	 * @param keyMap
	 * @return base64后的公钥
	 * @throws Exception
	 */
	public static String getPublicKey(Map<String, Object> keyMap)
			throws Exception {
		Key key = (Key) keyMap.get(PUBLIC_KEY);

		return encryptBASE64(key.getEncoded());
	}

	/**
	 * <p>初始化密钥.</p>
	 * <p>作者  [mihuajun,...]</p>
	 * <p>创建时间  2016年1月6日下午3:07:04</p>
	 * @return 公私钥对
	 * @throws Exception
	 */
	public static Map<String, Object> initKey() throws Exception {
		KeyPairGenerator keyPairGen = KeyPairGenerator
				.getInstance(KEY_ALGORITHM);
		keyPairGen.initialize(1024);

		KeyPair keyPair = keyPairGen.generateKeyPair();

		// 公钥
		RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();

		// 私钥
		RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();

		Map<String, Object> keyMap = new HashMap<String, Object>(2);

		keyMap.put(PUBLIC_KEY, publicKey);
		keyMap.put(PRIVATE_KEY, privateKey);
		return keyMap;
	}
	
}
