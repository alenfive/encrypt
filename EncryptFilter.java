package com.source3g.hermes.filter;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Base64Utils;
import org.springframework.web.filter.OncePerRequestFilter;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.source3g.hermes.utils.AESUtils;
import com.source3g.hermes.utils.RSAUtils;
import com.source3g.hermes.utils.WrapperedRequest;
import com.source3g.hermes.utils.WrapperedResponse;

public class EncryptFilter extends OncePerRequestFilter {

	private final String AES_KEY_NAME = "AES_KEY";
	private final String RSA_KEY_NAME = "RSA_KEY";
	private final String CIPHERTEXT_NAME = "ciphertext";

	private final String GET_RSA_KEY_ACTION = "GET_RSA_KEY";
	private final String SEND_AES_KEY_ACTION = "SEND_AES_KEY";
	private final String SEND_CIPHERTEXT_ACTION = "SEND_CIPHERTEXT";
	private final String ENCRYPT_ACTION = "ENCRYPT";
	
	private final String ENCRYPT_SUCCESS = "ENCRYPT_SUCCESS";
	private final String ENCRYPT_FAIL = "ENCRYPT_FAIL";
	

	/*private final Logger logger = Logger.getLogger(EncryptFilter.class);*/
	
	private ObjectMapper mapper = new ObjectMapper();

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException,
			IOException {
		// TODO Auto-generated method stub
		
		
		WrapperedRequest wrapperedRequest = new WrapperedRequest(request);
		
		String encryptAction = wrapperedRequest.getParameter(ENCRYPT_ACTION);

		// 非加密数据
		if (StringUtils.isEmpty(encryptAction)) {
			filterChain.doFilter(wrapperedRequest, response);
			return;
		}
		
		// 获取rsa公钥
		if (GET_RSA_KEY_ACTION.equals(encryptAction)) {
			getRsa(wrapperedRequest, response, filterChain);
		}else

		// 接受aes密钥
		if (SEND_AES_KEY_ACTION.equals(encryptAction)) {
			sendAes(wrapperedRequest, response, filterChain);
		}else
			
		// 接受并处理aes加密后的数据
		if (SEND_CIPHERTEXT_ACTION.equals(encryptAction)) {
			encrypt(wrapperedRequest, response, filterChain);
		}

	}
	
	private final void getRsa(WrapperedRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException{
		String public_key = null;
		String private_key = null;
		try {
			Map<String, Object> keys = RSAUtils.initKey();
			public_key = RSAUtils.getPublicKey(keys);
			private_key = RSAUtils.getPrivateKey(keys);
			request.getSession().setAttribute(RSAUtils.PUBLIC_KEY, public_key);
			request.getSession().setAttribute(RSAUtils.PRIVATE_KEY, private_key);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PrintWriter writer = response.getWriter();
		writer.print(RSA_KEY_NAME+"_"+public_key);
		writer.flush();
		writer.close();
		return;
	}
	
	private final void sendAes(WrapperedRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException{
		String aesKey = request.getParameter(AES_KEY_NAME);
		if(StringUtils.isEmpty(aesKey)){
			logger.debug("param AES_KEY_NAME is empty");
			return;
		}
		
		String private_key = (String)request.getSession().getAttribute(RSAUtils.PRIVATE_KEY);
		
		PrintWriter writer = response.getWriter();
		if(StringUtils.isEmpty(private_key)){
			logger.debug("private_key is invalid .return FAIL");
			writer.print(ENCRYPT_FAIL);
		}else{
			byte[] data;
			try {
				data = RSAUtils.decryptByPrivateKey(Base64Utils.decode(aesKey.getBytes("UTF-8")), private_key);
				aesKey = new String(data,"UTF-8");
				request.getSession().setAttribute(AES_KEY_NAME, aesKey);
				request.getSession().removeAttribute(RSAUtils.PRIVATE_KEY);
				writer.print(ENCRYPT_SUCCESS);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				writer.print(ENCRYPT_FAIL);
			}
		}
		
		writer.flush();
		writer.close();
		return;
	}

	private final void encrypt(WrapperedRequest request, HttpServletResponse response, FilterChain filterChain) throws JsonParseException, JsonMappingException, IOException, ServletException{
		String aesKey = (String) request.getSession().getAttribute(AES_KEY_NAME);
		Map<String, Object> result = new HashMap<String, Object>();
		if(StringUtils.isEmpty(aesKey)){
			logger.debug("AES_KEY is not exists");
			result.put("status", ENCRYPT_FAIL);
			response.getWriter().print(mapper.writeValueAsString(result));
			response.getWriter().flush();
			return;
		}
		String ciphertext = request.getParameter(CIPHERTEXT_NAME);
		//解密
		byte[] decryptFrom = AESUtils.parseHexStr2Byte(ciphertext);  
		byte[] decryptResult = AESUtils.decrypt(decryptFrom,aesKey);  
		Map<String, Object> mapParam = mapper.readValue(decryptResult,new TypeReference<Map<String, Object>>(){});
		
		request.setParameter(mapParam);
		WrapperedResponse wrapperedResponse = new WrapperedResponse(response);
		filterChain.doFilter(request, wrapperedResponse);
		
		logger.info("encrypt ："+new String(wrapperedResponse.getResponseData(),"UTF-8")); 
		
		//加密数据
		wrapperedResponse.flushBuffer();
		byte[] encryptResult = AESUtils.encrypt(new String(wrapperedResponse.getResponseData(),"UTF-8"), aesKey);  
		String encryptResultStr = AESUtils.parseByte2HexStr(encryptResult);  
		
		result.put("status", ENCRYPT_SUCCESS);
		result.put(CIPHERTEXT_NAME, encryptResultStr);
		
		String resultJson = mapper.writeValueAsString(result);
		response.setContentType("text/html;charset=utf-8"); 
		response.setContentLength(resultJson.length());
		
		logger.debug("encryptFilter result:"+resultJson);
		
		response.getWriter().print(resultJson);
		response.getWriter().flush();
		response.flushBuffer();
		wrapperedResponse.setContentLength(resultJson.length());
		return;
	}
}