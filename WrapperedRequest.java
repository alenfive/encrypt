package com.source3g.hermes.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class WrapperedRequest extends HttpServletRequestWrapper {

	private ObjectMapper mapper = new ObjectMapper();
	
	Map<String, Object> parameter = new HashMap<String, Object>();
	
	@SuppressWarnings("unchecked")
	public WrapperedRequest(HttpServletRequest request) {
		super(request);
		// TODO Auto-generated constructor stub
		if(request.getContentType().indexOf("application/json") < 0){
			//parameter.putAll(request.getParameterMap());
			initParameter(request.getParameterMap());
		}else{
			byte[] b = new byte[request.getContentLength()];
			try {
				request.getInputStream().read(b);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				parameter.putAll((Map<? extends String, ? extends Object>) mapper.readValue(b, new TypeReference<Map<String, Object>>() {}));
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
	}

	
	@Override
	public String getParameter(String name) {
		// TODO Auto-generated method stub
		
		if(parameter == null || parameter.get(name)==null)return null;
		
		Object v = parameter.get(name);
		
		if(v instanceof String[]){
			String[] vs = (String[])v;
			if(vs.length>0){
				return vs[0];
			}else{
				return null;
			}
		}else if (v instanceof String) {
			return (String) v;
		} else {
			return v.toString();
		}
	}

	@Override
	public Map<String, String[]> getParameterMap() {
		// TODO Auto-generated method stub
		
		Map<String, String[]> result = new HashMap<String, String[]>();
		
		Set<String> keys = parameter.keySet();
		
		for(String key : keys){
			Object v = parameter.get(key);
			if(v instanceof String[]){
				result.put(key, (String[])v);
			}else if(v instanceof String){
				result.put(key, new String[]{(String)v});
			}else{
				result.put(key, new String[]{v.toString()});
			}
		}
		
		return result;
	}


	@Override
	public String[] getParameterValues(String name) {
	 // TODO Auto-generated method stub
		super.getParameterValues(name);
		Object v = parameter.get(name);
		
		if(v == null)return null;
		
		if(v instanceof String[]){
			return (String[])v;
		}else if(v instanceof String){
			return new String[]{(String)v};
		}else{
			return new String[]{v.toString()};
		}
	}

	

	/**
	 * <p>方法说明概要.</p>
	 * <p>方法说明详细</p>
	 * <p>作者  [mihuajun,...]</p>
	 * <p>创建时间  Apr 5, 20164:49:38 PM</p>
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Enumeration<String> getParameterNames() {
		// TODO Auto-generated method stub
		Vector l = new Vector(parameter.keySet());
		return l.elements();
	}


	/**
	 * @return parameter
	 */
	public Map<String, Object> getParameter() {
		return parameter;
	}


	@Override
	public ServletInputStream getInputStream() throws IOException {
		// TODO Auto-generated method stub
		final ByteArrayInputStream buff = new ByteArrayInputStream(mapper.writeValueAsBytes(parameter));
		return new ServletInputStream() {
			
			@Override
			public int read() throws IOException {
				// TODO Auto-generated method stub
				return buff.read();
			}
		};
		
	}

	private void initParameter(Map<String, String[]> requestParam){
		this.parameter.clear();
		Set<String> keys = requestParam.keySet();
		for(String key : keys){
			String[] v = requestParam.get(key);
			if(v.length==1){
				parameter.put(key, v[0]);
			}else if(v.length > 0){
				parameter.put(key, v);
			}
		}
	}

	public void setParameter(Map<String, Object> requestParam) {
		// TODO Auto-generated method stub
		this.parameter.clear();
		Set<String> keys = requestParam.keySet();
		for(String key : keys){
			Object v = requestParam.get(key);
			if(v == null)continue;
			if(v instanceof List){
				List list = ((List)v);
				if(list.size()>1){
					String[] ls = null;
					this.parameter.put(key, list.toArray(ls));
				}else if(list.size()==1){
					this.parameter.put(key, list.get(0));
				}
			}else if(v instanceof String){
				this.parameter.put(key, (String)v);
			}else{
				this.parameter.put(key, v.toString());
			}
		}
	}
	
}
